<?php

namespace App\Http\Controllers;

use App\Call;
use App\Http\Requests\StoreCall;
use Illuminate\Http\Request;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calls = Call::with('user')->paginate(15);
        return view('calls.index_calls', compact('calls'));
    }

    /**
     * Show the form for creating a new resource.
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($user_id)
    {
        return view('calls.create_call', compact('user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCall $request)
    {
        $call = new Call();
        $call->fill($request->all());
        try{
            $call->save();
            return redirect()->route('user.calls',['user' => $call->user_id]);
        }catch(\Exception $e){
            return back()->withError($e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Call $call)
    {
        try{
            $call->delete();
            return back();
        }catch(\Exception $e){
            return back()->withError($e->getMessage())->withInput();
        }
    }
}
