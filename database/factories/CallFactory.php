<?php

use Faker\Generator as Faker;

$factory->define(App\Call::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomElement(App\User::pluck('id', 'id')->toArray()),
        'description' => $faker->text(255),
        'created_at' => $faker->dateTimeBetween('-2 month', 'now'),
        'status' => 'active'
    ];
});
