<?php

use Illuminate\Database\Seeder;

class CallTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Call::class, 153)->create();
    }
}
