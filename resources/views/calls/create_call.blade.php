@extends('layout')
@section('content')
  <div class="container">
    <div class="d-flex flex-column">
      <h1>Create new call</h1>
      @if(!empty($errors->first()))
        <div class="d-flex flex-column">
          <div class="alert alert-danger">
            <span>{{ $errors->first() }}</span>
          </div>
        </div>
      @endif
      @if (session('error'))
        <div class="d-flex flex-column">
          <div class="alert alert-danger">{{ session('error') }}</div>
        </div>
      @endif
      {!! Form::open(['url'=>route('calls.store'), 'class'=>'form-horizontal','method' => 'POST']) !!}
      <div class="form-group">
        {!! Form::label('description', 'Your text', array('class' => 'awesome')) !!}
        {!! Form::textarea('description', '',[ 'class' => 'form-control']) !!}
      </div>
      {!! Form::hidden('user_id', $user_id) !!}
      <div class="form-group">
        {!! Form::submit('Save',['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>

@endsection