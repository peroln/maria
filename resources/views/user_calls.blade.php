@extends('layout')
@section('content')

  <div class="container">
    <div class="d-flex flex-column mb-5 mt-5">
      <h3>User name is {{$user->name}}</h3>
    </div>
    <div class="d-flex flex-column">
      <table class="table table-bordered">
        <thead>
        <tr>
          <th scope="col" class="text-center">ID</th>
          <th scope="col">Description</th>
          <th scope="col">Status</th>
          <th scope="col">Created At</th>
          <th scope="col" class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($calls as $call)
          <tr>
            <th scope="row" class="text-center">{{$call->id}}</th>
            <td>{{$call->description}}</td>
            <td>{{$call->status}}</td>
            <td>{{$call->created_at}}</td>
            <td class="text-center">
              {!! Form::open(['url'=>route('calls.destroy',['call'=>$call->id]), 'class'=>'form-horizontal','method' => 'POST']) !!}
              {{method_field('delete')}}
              {!! Form::button('Delete',['class'=>'btn btn-danger','type'=>'submit']) !!}
              {!! Form::close() !!}
              </a>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    <div class="d-flex flex-column">
      {{ $calls->links() }}
    </div>
    <div class="d-flex flex-row">
      <a class="btn btn-primary" href="{{route('calls.create',['user_id' => $user->id])}}" role="button">Add new
        call</a>
    </div>

  </div>
@endsection