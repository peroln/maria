<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index')->name('home');
Route::get('/user/{user}/calls', 'UserController@calls' )->name('user.calls');
Route::resource('calls', 'CallController',['except' => 'create']);
Route::get('user/{user_id}/calls/create',['as' => 'calls.create', 'uses' => 'CallController@create']);