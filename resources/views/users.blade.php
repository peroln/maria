@extends('layout')
@section('content')
  <div class="container">
    <div class="d-flex flex-column mb-5">
      <h3>List users</h3>
    </div>
    <div class="d-flex flex-column">
        <table class="table table-bordered">
          <thead>
          <tr>
            <th scope="col" class="text-center">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Created At</th>
            <th scope="col" class="text-center">History calls</th>
          </tr>
          </thead>
          <tbody>
          @foreach($users as $user)
            <tr>
              <th scope="row" class="text-center">{{$user->id}}</th>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->created_at}}</td>
              <td class="text-center">
                <a href="{{ route('user.calls',['user' => $user->id]) }}">
                <ion-icon name="call"></ion-icon>
                </a>
              </td>
            </tr>
          @endforeach

          </tbody>
        </table>

    </div>
    <div class="d-flex flex-column">
      {{ $users->onEachSide(2)->links() }}
    </div>
  </div>

@endsection